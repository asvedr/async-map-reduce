use std::any::Any;
use std::thread::JoinHandle;

pub type ThreadError = Box<dyn Any + Send + 'static>;
pub enum MapReduceError<E> {
    ThreadFailed(ThreadError),
    Custom(E),
}

pub(crate) struct CtxWrapper {
    data: *const u8,
}

impl Clone for CtxWrapper {
    fn clone(&self) -> Self {
        CtxWrapper { data: self.data }
    }
}

impl Copy for CtxWrapper {}

unsafe impl Send for CtxWrapper {}
unsafe impl Sync for CtxWrapper {}

impl CtxWrapper {
    pub fn new<T>(value: &T) -> CtxWrapper {
        CtxWrapper {
            data: value as *const T as *const u8,
        }
    }

    pub fn get<T>(&self) -> &T {
        unsafe { &*(self.data as *const T) }
    }
}

pub(crate) struct Worker<Resp> {
    pub thread: Box<JoinHandle<Resp>>,
}

pub(crate) struct ResWrapper<T> {
    data: Box<T>,
}

unsafe impl<T> Send for ResWrapper<T> {}
unsafe impl<T> Sync for ResWrapper<T> {}

impl<T> ResWrapper<T> {
    pub(crate) fn new(val: T) -> Self {
        Self {
            data: Box::new(val),
        }
    }

    pub(crate) fn get(self) -> T {
        *self.data
    }
}
