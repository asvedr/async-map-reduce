pub trait Sss: 'static {}
pub trait SCFunMF<Ctx, Req, Resp>: 'static + Fn(&Ctx, usize, &Req) -> Resp {}
pub trait SCFunRF<Ctx, Resp>: 'static + Fn(&Ctx, Resp, Resp) -> Resp {}

impl<T: 'static> Sss for T {}
impl<A, B, C, T: 'static + Fn(&A, usize, &B) -> C> SCFunMF<A, B, C> for T {}
impl<A, B, T: 'static + Fn(&A, B, B) -> B> SCFunRF<A, B> for T {}
