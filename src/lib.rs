pub mod entities;
pub mod managers;
pub(crate) mod traits;

pub use entities::MapReduceError;
pub use managers::{
    manager, FullManager, FullRManager, Manager, MapManager, ReduceManager, ReduceRManager,
};
